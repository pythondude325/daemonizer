#!/usr/bin/bash

# TODO:
# - Make installer
# - Add information about terminating socat in start script
# - Add error handleing
# - Add lockfile

SOCKET="<socket location>"
START_SCIRPT="<your start script here>"
STOP_SCIRPT="<your stop script here>"

SCRIPT_NAME="$0"

print_usage(){
	echo
	echo "Usage:"
	echo "$0 <command> <args> ..."
	echo "    Run command"
	echo "    Command may be: start, stop, connect, send"
	echo "$0 help"
	echo "    Print this message and exit"
	echo "$0 help <command>"
	echo "    Show more information on the command"
	echo
}

print_command_usage(){
	echo
	echo "Usage:"
	echo "$0 $1"
	echo
	echo "$2"
	echo 
}

start_server(){
	nohup socat "EXEC:$START_SCRIPT" "UNIX-LISTEN:$SOCKET,fork" &
}

stop_server(){
	socat "EXEC:$STOP_SCRIPT" "UNIX-CONNECT:$SOCKET"
}

send_text(){
	echo "$*" | socat - "UNIX-CONNECT:$SOCKET"
}

connect(){
	socat "READLINE" "UNIX-CONNECT:$SOCKET"
}

case $1 in
	start)
		start_server
		;;
	stop)
		stop_server
		;;
	restart)
		stop_server
		start_server
		;;
	connect)
		connect
		;;
	send)
		shift
		send_text "$*"
		;;
	help)
		case $2 in
			start)
				print_command_usage "start" "Starts the daemon process"
				;;
			stop)
				print_command_usage "stop" "Stops the daemon process"
				;;
			connect)
				print_command_usage "connect" "Opens an interactive communication with the process"
				;;
			send)
				print_command_usage "send <text> ..." "Sends the supplied text to the daemon then exits"
				;;
			"")
				print_usage
				;;
			*)
				echo "There is no command called '$2'"
				echo "The available commands are: start, stop, connect, send"
				exit 1
				;;
		esac
		;;
	"")
		echo "You must supply a command."
		print_usage
		exit 1
		;;
	*)
		echo "'$1' is not a valid command."
		print_usage
		exit 1
		;;
esac
	
